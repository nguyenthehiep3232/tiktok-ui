import Home from "~/pages/Home";
import Follow from "~/pages/Follow";
import Profile from "~/pages/Profile";
import Upload from "~/pages/Upload";
import { HeaderOnly } from "~/components/Layout";
import Search  from "~/pages/Search";

const routePublic = [
  {path: '/', component: Home },
  {path: '/follow', component: Follow},
  {path: '/profile', component: Profile },
  {path: '/upload', component: Upload, layout: HeaderOnly },
  {path: '/search', component: Search, layout: null },
];

// user must login to access
const routePrivate = [

];

export { routePublic, routePrivate };
